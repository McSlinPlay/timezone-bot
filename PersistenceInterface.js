const fs = require('fs')

class PersistenceInterface {

	constructor(fileName) {
		if (fileName === undefined || fileName === null) {
			throw new Error('fileName must be supplied for PersistenceInterface to work')
		}
		this.fileName = __dirname + '/' + fileName
	}

	/*getUserPref(user) {
		console.log('try getting')
		let buffer = fs.readFileSync(this.fileName).toString().split('\n')
		let line = ""
		for (const bufferElement of buffer) {
			console.log(bufferElement)
			if (bufferElement.startsWith(user.username + "#" + user.discriminator)) {
				console.log('found', bufferElement)
				line = bufferElement
			}
		}
		return line.split('=')[1]
	}*/

	getUserPref(user) {
		console.log('try getting')
		let buffer = this.#getFileArray(this.#getPrefFile())
		let lineForUserPref = this.#getLineForUserPref(user, buffer)
		if (lineForUserPref === -1) {
			return undefined
		}
		return buffer[lineForUserPref].split('=')[1]
	}

	#getLineForUserPref = (user, array) => {
		let line = -1
		for (let i = 0; i < array.length; i++) {
			console.log(array[i])
			if (array[i].startsWith(user.username + '#' + user.discriminator + '=')) {
				console.log('found', array[i])
				line = i
			}
		}
		return line
	}

	#getFileArray = (data) => {
		return data.toString().split('\n')
	}

	#getPrefFile = () => {
		return fs.readFileSync(this.fileName)
	}


	setUserPref(user, region) {
		const newString = `${user.username}#${user.discriminator}=${region}`
		let file = this.#getPrefFile();
		let fileArray = this.#getFileArray(file)
		let lineForUserPref = this.#getLineForUserPref(user, fileArray)
		if (lineForUserPref === -1) {
			console.log('Adding new')
			fs.appendFileSync(this.fileName, `\n${user.username}#${user.discriminator}=${region}`)
			return
		}
		let line = fileArray[lineForUserPref]
		let replace = file.toString().replace(new RegExp(line), newString)
		fs.writeFileSync(this.fileName, replace)
	}

	#getLineForUserBlock = (author, fileArray) => {
		let line = -1
		for (let i = 0; i < fileArray.length; i++) {
			if (fileArray[i].startsWith(author.username + '#' + author.discriminator + ':blocked')) {
				line = i
			}
		}
		return line
	}

	blockUser = (author) => {
		const newString = `${author.username}#${author.discriminator}:blocked`
		if (!this.isBlocked(author)) {
			fs.appendFileSync(this.fileName, '\n' + newString)
		}
	}

	isBlocked = (author) => {
		let prefFile = this.#getPrefFile()
		let fileArray = this.#getFileArray(prefFile)
		let line = this.#getLineForUserBlock(author, fileArray)
		if (line === -1) {
			return false
		}
		return true
	}

	unblockUser = (author) => {
		const newString = `${author.username}#${author.discriminator}:blocked`
		if (this.isBlocked(author)) {
			let file = this.#getPrefFile().toString()
			let replace = file.toString().replace(new RegExp(newString), '')
			fs.writeFileSync(this.fileName, replace)
		}
	}
}

module.exports = PersistenceInterface