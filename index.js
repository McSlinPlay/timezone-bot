const Discord = require('discord.js')
const config = require('./config.json')
const packageJSON = require('./package.json')
const moment = require('moment-timezone')
const {Expression} = require('javascript-calc-interpreter')

const PersistenceInterface = require('./PersistenceInterface')
const persistence = new PersistenceInterface('userdata.txt')
const client = new Discord.Client()

const commandPrefix = ';;'


/**
 * supply a footer message with the default part plus an additional message
 * @param additional
 * @returns {string}
 */
const supplyFooter = (additional = commandPrefix + 'help') => {
	return `-- created by McSlinPlay#5276 -- ${additional} --`
}

/**
 * Filters an array to remove all empty values
 * @param array to filter
 * @param predicate which items to filter for in addition to predefined empty values
 * @returns {*}
 */
const arrayFilterEmpty = (array, predicate = (_) => true) => {
	return array.filter(value => {
		return value !== undefined && value !== null && value !== '' && predicate(value)
	})
}

/**
 * Splits a command send by an user into useful parts. Which command he invoked and with which arguments
 * @param command
 * @returns {{arguments: null, command: null}|{arguments: *, command: *}}
 */
const splitCommand = (command) => {
	if (command === commandPrefix) {
		return {
			command: null,
			arguments: null,
		}
	}
	let split = command.split(commandPrefix)
	let filterEmpty = arrayFilterEmpty(split, value => {
		return value !== ' ' && value !== commandPrefix
	})
	if (filterEmpty.length === 0) {
		return {
			command: null,
			arguments: null,
		}
	}
	let split1 = filterEmpty[0].split(/ (.+)/)
	let filterEmpty1 = arrayFilterEmpty(split1)
	console.log(filterEmpty1)
	return {
		command: filterEmpty1[0],
		arguments: filterEmpty1[1],
	}
}

client.on('message', function (message) {
	/**
	 * Don't respond to other bots
	 */
	if (message.author.bot) {
		return
	}
	/**
	 * Check if message is an command and if so split it and process it
	 */
	if (message.content.startsWith(commandPrefix) || persistence.isBlocked(message.author)) {
		// Split the entered message to get the command and arguments
		const {command, arguments} = splitCommand(message.content)
		console.log(command, arguments)
		// If the command was help display the help message
		if (persistence.isBlocked(message.author)) {
			if (command === 'unblock') {
				persistence.unblockUser(message.author)
				if (!persistence.isBlocked(message.author)) {
					message.react('👌')
				}
			}
		} else if (command === 'help') {
			const reply = new Discord.MessageEmbed()
				.setColor('GREEN')
				.setTitle('TimezoneBot by McSlinPlay (v' + packageJSON.version + ')')
				.setDescription('This Bot automatically translates between timezones/regions. If you write a time' +
					' in your message, the bot will reply with the translated time. If you don\'t want to use your' +
					' default region you can also specify one after the time. Look at the `;;list` command' +
					' \nThe most useful commands:')
				.addField('List Regions', '`;;list ([searchTerm])`')
				.addField('Set your default timezone', '`;;set [region]`')
				.addField('Block the bot from replying to your messages', '`;;block`')
				.addField('Unblock the bot from replying to your messages', '`;;unblock`')
				.setFooter(supplyFooter('"[]" are arguments, "()" are optional'))
			// .setThumbnail("https://i.ibb.co/nDNMdN1/Schau-dir-mein-Canva-Design-an.png")
			message.reply(reply)
		} else if (command === 'list') {
			const searchTerm = arguments
			console.log(`Search: ${searchTerm}`)
			const allNames = moment.tz.names()
			const searchResult = allNames.filter(name =>
				name.includes(searchTerm),
			)
			const searchResults = searchResult.length
			let splice = searchResult.splice(0, 15)
			const displayedSearchResults = splice.length
			splice = splice.map(value => '- ' + value)
			let join1 = splice.join('\n')
			let countMessage
			if (searchResults > displayedSearchResults) {
				countMessage = `${displayedSearchResults} of ${searchResults} results shown`
			}
			if (searchResults === 0) {
				join1 = 'No results found for your request.'
			}
			console.log(join1)
			let messageEmbed = new Discord.MessageEmbed()
				.setTitle(`Results for search:`)
				.setDescription(join1)
			if (countMessage !== undefined) {
				messageEmbed.setFooter(supplyFooter(countMessage))
			} else {
				messageEmbed.setFooter(supplyFooter())
			}
			message.reply(messageEmbed)
			// console.log(allNames, searchResult)
		} else if (command === 'set') {
			console.log('setting user region to "' + arguments + '"')
			if (moment.tz.names().includes(arguments)) {
				console.log('region exists')
				console.log(`${message.author.username}#${message.author.discriminator}=${arguments}`)
				persistence.setUserPref(message.author, arguments)
				message.react('👌')
			} else {
				console.log('region does not exist')
				message.reply('Region does not exist')
			}
		} else if (command === 'block') {
			persistence.blockUser(message.author)
			message.reply(`The bot will not longer reply to your messages. If you ever change your mind you can reverse this with \`${commandPrefix}unblock\`.`)
		} else if (command === 'bing') {
			message.reply('bong')
		} else if (command === 'calc') {
			console.log('Args', arguments)
			if (arguments === undefined || arguments === '') {
				let embed = new Discord.MessageEmbed()
					.setTitle('Help')
				Expression.getHelp().forEach(value => embed.addField(value.command, value.description))
				message.reply(embed)
			} else if (arguments === 'constants') {
				let title = new Discord.MessageEmbed().setTitle('Constants')
				let constants = Expression.getConstants()
				for (let constantsKey in constants) {
					title.addField(constantsKey, constants[constantsKey])
				}
				message.reply(title)
			} else {
				// let parse = calcInterpreter.parse(arguments)
				try {
					let content = Expression.evaluate(arguments, message.author)
					console.log(content)
					message.reply(content)
				} catch (e) {
					message.reply(e.message)
				}
			}
		}
	}
	/**
	 * If not check if the message contains a time and display the timezone converter message
	 */
	else {
		// search for time and timezone
		// language=RegExp
		let regExpMatchArray = message.content.match(/;(\d{1,2})(((:\d{2})?(:\d{2})? ?(pm|am|PM|AM))|(:\d{2})) ?(\w+\/([a-zA-Z_]+\/)?[a-zA-Z_]+)?/)
		if (regExpMatchArray) {
			let regExpMatchArrayElement = regExpMatchArray[0]
			console.log(regExpMatchArray)
			let split = regExpMatchArrayElement.substr(1).split(/( |:|pm|am|PM|AM)/)
			const regionMatch = /(\w+\/([a-zA-Z_]+\/)?[a-zA-Z_]+)/
			const pmAmMatch = /(am|AM|pm|PM)/
			const pmMatch = /(pm|PM)/
			const amMatch = /(am|AM)/
			let pmAm = split.filter(el => {
				return el.match(pmAmMatch)
			})[0]
			let filter = split.filter(el => {
				return el !== '' && el !== ' ' && el !== ':' && !el.match(pmAmMatch)
			})
			let region = ''
			if (filter[filter.length - 1].match(regionMatch)) {
				region = filter[filter.length - 1]
				filter.splice(filter.length - 1)
			}
			console.log(region)
			console.log(pmAm)
			console.log(filter)
			let tempDate = new Date()
			if (filter[0].length === 1) {
				filter[0] = 0 + filter[0]
			}
			let dateString = tempDate.toLocaleDateString()
			let strings = dateString.split('-')
			for (let i = 0; i < strings.length; i++) {
				if (strings[i].length === 1) {
					strings[i] = 0 + strings[i]
				}
			}
			let isPM = false
			if (pmAm === 'pm' || pmAm === 'PM') {
				isPM = true
			}
			let isAM = false
			if (pmAm === 'am' || pmAm === 'AM') {
				isAM = true
			}
			let join = strings.join('-')
			let hour = parseInt(filter[0])
			if (isPM && hour < 12) {
				// console.log('is pm adding 12h')
				filter[0] = hour + 12
			} else if (isAM && hour === 12) {
				// console.log("12am convert to 00:00")
				filter[0] = hour - 12
			}
			if (filter[0].length === 1 || filter[0] === 0) {
				filter[0] = '0' + filter[0]
			}
			let s = `${join} ${filter[0]}`
			if (filter[1] !== undefined) {

				if (filter[1].length === 1) {
					s += 0
				}

				s += `:${filter[1]}`
			} else {
				s += ':00'
			}
			if (filter[2] !== undefined && filter[2].match(/\d{1,2}/)) {
				s += `:${filter[2]}`
			} else {
				s += ':00'
			}
			// const tokens2 = s.split(' ').slice(0, 4),
			// 	result2 = tokens2.join(' ')
			console.log(s)
			if (region === '' || region === undefined) {
				region = persistence.getUserPref(message.author)
			}
			console.log('region', region)
			if (region === '' || region === undefined) {
				region = 'UTC'
			}
			let tz = moment.tz(s, region)
			let utc = tz.utc()
			console.log(tz.format('YYYY-MM-DD HH-mm-ss z Z'))
			// console.log('time found', regExpMatchArray[0])
			const exampleEmbed = new Discord.MessageEmbed()
				.setFooter(`Time above (${utc.tz(region).format('HH:mm z')}) in your time`)
				.setTimestamp(utc)
			message.reply(exampleEmbed)
		}
	}
})

client.login(config.BOT_TOKEN).then(r => console.log(r))